package org.hou.graphql.repository;

import org.hou.graphql.entity.Vehicule;
import org.springframework.data.jpa.repository.JpaRepository;
// test MR
public interface VehiculeRepository extends JpaRepository<Vehicule, Integer>{

}
